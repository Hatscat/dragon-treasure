// eslint-disable-next-line @typescript-eslint/no-var-requires
const sveltePreprocess = require('svelte-preprocess');

module.exports = {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  preprocess: sveltePreprocess({
    // ...svelte-preprocess options (optional)
  }),
  // ...other svelte options (optional)
};
