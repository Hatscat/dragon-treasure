export interface Character {
  icon: string,
  active: boolean,
  score: number,
  loot: string,
}

export function createCharacter(props?: Partial<Character>): Character {
  return {
    icon: '🙂',
    active: false,
    score: 0,
    loot: '💍💍💍',
    ...props
  }
}
