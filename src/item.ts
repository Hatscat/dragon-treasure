export enum ItemKind {
  Dragon,
  Spider,
  Coin,
  Ring,
  Scepter,
  Diamond,
  Crown,
  Crystal,
}

interface ItemBase {
  type: string;
  icon: string;
  kind: ItemKind;
  maxCount: number;
}

export interface Monster extends ItemBase {
  type: 'monster';
  kind: ItemKind.Dragon | ItemKind.Spider;
}

export interface Treasure extends ItemBase {
  type: 'treasure';
  kind: ItemKind.Coin | ItemKind.Ring | ItemKind.Scepter | ItemKind.Diamond | ItemKind.Crown | ItemKind.Crystal;
  score: number;
}

export type Item = Monster | Treasure;

const createMonster = (props: Partial<Monster>): Monster => ({
  type: 'monster',
  kind: ItemKind.Dragon,
  icon: '🐉',
  maxCount: 0,
  ...props,
})

const createTreasure = (props: Partial<Treasure>): Treasure => ({
  type: 'treasure',
  kind: ItemKind.Coin,
  icon: '💰',
  maxCount: 0,
  score: 0,
  ...props,
})

export const Item: Record<ItemKind, Item> = Object.freeze({
  [ItemKind.Dragon]: createMonster({
    kind: ItemKind.Dragon,
    icon: '🐉',
    maxCount: 12,
  }),
  [ItemKind.Spider]: createMonster({
    kind: ItemKind.Spider,
    icon: '🕷',
    maxCount: 3,
  }),
  [ItemKind.Coin]: createTreasure({
    kind: ItemKind.Coin,
    icon: '💰',
    maxCount: 8,
    score: 1,
  }),
  [ItemKind.Ring]: createTreasure({
    kind: ItemKind.Ring,
    icon: '💍',
    maxCount: 4,
    score: 2,
  }),
  [ItemKind.Diamond]: createTreasure({
    kind: ItemKind.Diamond,
    icon: '💎',
    maxCount: 4,
    score: 2,
  }),
  [ItemKind.Scepter]: createTreasure({
    kind: ItemKind.Scepter,
    icon: '🧸',
    maxCount: 4,
    score: 2,
  }),
  [ItemKind.Crown]: createTreasure({
    kind: ItemKind.Crown,
    icon: '👑',
    maxCount: 6,
    score: 3,
  }),
  [ItemKind.Crystal]: createTreasure({
    kind: ItemKind.Crystal,
    icon: '🔮',
    maxCount: 8,
    score: 4,
  }),
})
