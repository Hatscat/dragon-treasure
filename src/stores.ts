import { writable } from 'svelte/store';
import { Character, createCharacter } from './character'
import { Cell, getRandomMap } from './cell'

function createCharacterStore() {
  const allCharacters: Character[] = [
    createCharacter({icon: '🤖'}),
    createCharacter({icon: '👽'}),
    createCharacter({icon: '🦊'}),
    createCharacter({icon: '👻'}),
    createCharacter({icon: '🧙‍♂️'}),
    createCharacter({icon: '🧝‍♀️'}),
  ]

  const { subscribe, update } = writable(allCharacters);

  return {
    subscribe,
    toggle: (character: Character) => update((state: Character[]) => {
      return state.map(c => {
        if (c.icon === character.icon) {
          c.active = !c.active
        }
        return c
      });
    }),
    setScore: (character: Character, score: number) => update((state: Character[]) => {
        return state.map(c => {
        if (c.icon === character.icon) {
          c.score = score
        }
        return c
      });
    }),
    findActiveFrom: (index: number) => {
      while (!allCharacters[index].active && index < allCharacters.length) {
        index += 1;
      }
      return allCharacters[index];
    }
  };
}
export const characters = createCharacterStore();

export const cells = writable(getRandomMap() as Cell[]);

export enum Stage {
  CharacterSelection,
  Play,
  GameOver,
}
export const stage = writable(Stage.CharacterSelection);

export const characterTurn = writable(0);
