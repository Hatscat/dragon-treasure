import { ItemKind, Item } from './item'
import { shuffle } from './helpers'

export interface Cell {
  item: ItemKind | null,
  visible: boolean,
}

function createCell(props?: Partial<Cell>): Cell {
  return {
    item: null,
    visible: false,
    ...props
  }
}

export function getRandomMap(): Cell[] {
  return shuffle(Object.values(Item).reduce((cells, item) =>
    cells.concat(Array.from(Array(item.maxCount)).map(_ => createCell({ item: item.kind })))
    , []))
}

